<?php
session_start();
include('model/database.php');
include("controller/cartController.php");
include('cart_count.php');
?>
<!DOCTYPE html>
<html>
<head>
<style>

div.ex1 {
  background-color: lightblue;
  width: auto;
  height: 500px;
  overflow: auto;
}

</style>
<?php include('views/template/header.php');?>
<title>HOME</title>
</head>
<body>
	<?php 
		
		include("controller/userProfileController.php");
		include("views/template/nav.php");
		include("views/cover.php");
		include("views/special.php");
		
		include("views/sellerproduct.php");
		include("views/extraspecial.php");
		include("views/testominal.php");
		include("views/comment.php");
		include("views/template/footer.php");
	?>
</body>
</html>
