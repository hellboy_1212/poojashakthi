<section class="counters1 counters cid-rrbv9kNNaF" id="counters1-7">
		<div class="container">
			<div class="container pt-4 mt-2">
				<div class="media-container-row">
					<div class="card p-3 align-center col-12 col-md-6 col-lg-4">
						<div class="panel-item p-3">
							<div class="card-img pb-3">
								<span class="mbr-iconfont mobi-mbri-success mobi-mbri" style="color: rgb(255, 60, 60); fill: rgb(255, 60, 60);"></span>
							</div>
							<div class="card-text">
								<h3 class="count pt-3 pb-3 mbr-fonts-style display-5">35</h3>
								<h4 class="mbr-content-title mbr-bold mbr-fonts-style display-7">
									Genuine Products</h4>
								<p class="mbr-content-text mbr-fonts-style display-7">
									Mobirise give you the freedom to develop as many websites as you like given the fact that it is desktop app. Mobirise is the perfect gide.
								</p>
							</div>
						</div>
					</div>


					<div class="card p-3 align-center col-12 col-md-6 col-lg-4">
						<div class="panel-item p-3">
							<div class="card-img pb-3">
								<span class="mbr-iconfont mobi-mbri-shopping-bag mobi-mbri" style="color: rgb(255, 60, 60); fill: rgb(255, 60, 60);"></span>
							</div>
							<div class="card-text">
								<h3 class="count pt-3 pb-3 mbr-fonts-style display-5">100</h3>
								<h4 class="mbr-content-title mbr-bold mbr-fonts-style display-7">
									Trusted Providers</h4>
								<p class="mbr-content-text mbr-fonts-style display-7">
									Choose from the large selection of latest pre-made blocks - jumbotrons, hero images, parallax scrolling, video backgrounds, h</p>
							</div>
						</div>
					</div>

					<div class="card p-3 align-center col-12 col-md-6 col-lg-4">
						<div class="panel-item p-3">
							<div class="card-img pb-3">
								<span class="mbr-iconfont mobi-mbri-smile-face mobi-mbri" style="color: rgb(255, 60, 60); fill: rgb(255, 60, 60);"></span>
							</div>
							<div class="card-text">
								<h3 class="count pt-3 pb-3 mbr-fonts-style display-5">
									1000</h3>
								<h4 class="mbr-content-title mbr-bold mbr-fonts-style display-7">
									Happy Costumers</h4>
								<p class="mbr-content-text mbr-fonts-style display-7">
									One of Bootstrap's big point is responsiveness and obirise male effective use of this by genereating highly responsive website for you.
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>