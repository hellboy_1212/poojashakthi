<hr>
<h1><p><b style="color:green" class="text-align">FEATURE PRODUCT</b></p></h1>
<p class="text-align">Choose your Product</p>

	<main class="mt-5 pt-4">
		<?php
			$result = mysqli_query($conn,"SELECT * FROM `products` where feature = 1");
			while($row = mysqli_fetch_assoc($result)){
			echo'
				<form method="post" enctype="multipart/form-data" >
				<input type="hidden" name="product_id" value='.$row["product_id"].' /> 
				<input type="hidden" name="prices" value='.$row["product_price"].' /> 
					<table>
				<tr>
					
					<td rowspan="2"><img src="admin-098/views/images/'.$row["product_image"].'" class="img-fluid" height="200px" width="200px"></td>
					<td>'.$row["product_name"].'</td>	
				</tr>
				<tr>
					<td>
						<p class="lead">Rs.
							'.$row["product_price"].'
							<span class="mr-1">
								<del>Rs. '.$row["product_actualprice"].'</del>
							</span>
						</p>
	
						<p class="lead font-weight-bold" >Description</p>
	
						<p>'.$row["product_description"].'</p></td>
				</tr>
				<tr>
					<td colspan="2"><div style="text-align: center">';
							
					if(!empty($_SESSION['id'])){ 
						echo '
						<button name="addToCart" class="btn btn-primary btn-sm my-0 p" type="submit">Add to cart
						<i class="fas fa-shopping-cart ml-1"></i></button>
					';}else{ 
						echo '';
					} 
			echo'		
					</div></td>
				</tr>
				</table>
				<b><hr></b>
		
			</form>';}
			?>
				<table>
				<tr>
				<td colspan="2">
				<hr>
					<div class="row d-flex justify-content-center wow fadeIn">
						<div class="col-md-6 text-center">
							<h3><u class="my-6 h4">Additional information about Rudraksha</u></h3>
							<p>Rudraksha (Rudrākṣa) is a seed that is used as a prayer bead in Hinduism,
							 especially in Shaivism. When they are ripe, Rudraksha seeds are covered by a blue outer shell 
							 and are sometimes called blueberry beads.The seeds are produced by several species of large, 
							 evergreen, broad-leaved tree in the genus Elaeocarpus, the principle of which is Elaeocarpus ganitrus roxb.
							 The seeds are associated with the Hindu deity Shiva and are commonly worn for protection and for chanting 
							 mantras such as Om Namah Shivaya. The seeds are primarily used n India, Indonesia and Nepal as beads for organic 
							 jewellery and malas; they are valued similarly to semi-precious stones. Various meanings and
							 attributed to beads with different numbers of segments (mukh), and rare or unique beads are highly prized and 
							 valuable.</p>
						</div>
					</div>
					<div class="row wow fadeIn">
						<div class="col-lg-4 col-md-12 mb-4">
							<img src="https://c8.alamy.com/comp/BA112J/japa-mala-set-of-beads-commonly-used-by-hindus-and-buddhists-on-a-BA112J.jpg"  height="250" width="300">
						</div>
						<div class="col-lg-4 col-md-6 mb-4">
							<img src="https://the-crystal-council.sfo2.digitaloceanspaces.com/prod/96/b185fa7d42e2eab22a96f70e618be893.jpg"  height="250" width="300">
						</div>
						<div class="col-lg-4 col-md-6 mb-4">
							<img src="https://img.timesnownews.com/story/1560670879-rudraksh.jpg"  height="250" width="300">
						</div>
					</div>
					</td>
				</tr>
				</table>
			</div>
		</div>
	</main>