<section class="mbr-section"
        style="background-image: linear-gradient(to right top, #e60000, #ee2721, #f53d38, #fb504d, #ff6161);">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-xl-9 mx-auto">
                    <div class="card card-signin flex-row my-5">
                        <div class="card-img-left d-none d-md-flex">
                        </div>
                        <div class="card-body">
                            <h2 class="mbr-section-title align-center pb-3 mbr-fonts-style display-2">
                                SIGN UP</h2>
                            <form action="controller/signupuser.php" class="form-signin"  method="post" onSubmit="return check();" name="form1">
                                <div class="form-label-group">
                                    <input type="text" name="user_firstName" class="form-control"
                                        placeholder="Firstname" required autofocus>
                                </div>

                                <div class="form-label-group">
                                    <input type="text" name="user_lastName" class="form-control"
                                        placeholder="Lastname" required autofocus>
                                </div>
                                
                                <div class="form-label-group">
                                    <input type="email" name="user_email" class="form-control"
                                        placeholder="Email" required autofocus>
                                </div>

                                <div class="form-label-group">
                                    <input type="number" name="user_phone" class="form-control"
                                        placeholder="Phone Number" required autofocus>
                                </div>

                                <div class="form-label-group">
                                    <input type="text" name="user_addres" class="form-control"
                                        placeholder="Address" required autofocus>
                                </div>

                                <div class="form-label-group">
                                    <input type="password" name="user_pass" class="form-control"
                                        placeholder="Password" id="myInput" minlength="6" maxlength="15" required autofocus>
                                        <input type="checkbox" onclick="myFunction()"> Show Password</td></tr>
                                </div>

                                <input type="submit" value="Register" name="submit" class="btn btn-lg btn-primary btn-block text-uppercase" >
                                <div>
                                <p>Already Have an Account? 
                                <a href="login.php">Sign In</a></p>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>