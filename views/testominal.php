<section class="testimonials1 cid-rrbJK9MoG1" id="testimonials1-9">
		<div class="container">
			<div class="media-container-row">
				<div class="title col-12 align-center">
					<h2 class="pb-3 mbr-fonts-style display-2">
						WHAT OUR FANTASTIC CUSTOMERS SAY
					</h2>
				</div>
			</div>
		</div>
	<div class="ex1">
		<div class="container pt-3 mt-2">
			<div class="col">
				<?php 
					$result = mysqli_query($conn,"SELECT * FROM `comment`");
					while($row = mysqli_fetch_assoc($result)){
					echo '
					<div class="form-label-group">
						<table>
							<tr>
								<th colspan="2" style="text-align: center;">'.$row["name"].'</th>
							</tr>
							<tr>
								<th></th>
								<td style="padding-right: -20px;"><i class="fas fa-user-circle" style="font-size: 50pt;"></i></td>
								<td>&nbsp;&nbsp;'.$row["detail"].'</td>
							</tr>
						</table>
					</div>';}
				?><br />
			</div>
		</div>
		</div>
	</section>
