<section class="shopping-cart dark"
            style="background-image: linear-gradient(to right top, #e60000, #ee2721, #f53d38, #fb504d, #ff6161);">
            <div class="container">
                <div class="block-heading">
                    <h2 class="pb-3 mbr-section-title mbr-fonts-style text-white" style="font-size: 40px;"><strong>
                            SHOPPING CART</strong></h2>
                    <?php
                        if(isset($_SESSION["shopping_cart"]))
                        {
                            $total_price = 0;
                            echo $status;
                    ?>
                           
                            <table class="table" style="padding: 30px">
                            
                                <tbody>
                                    <tr>
                                        <td>IMAGE</td>
                                        <td>ITEM NAME</td>
                                        <td>QUANTITY</td>
                                        <td>UNIT PRICE</td>
                                        <td>ITEMS TOTAL</td>
                                        <td colspan="6">REMOVE</td>
                                    </tr>
                                    <?php		
                                        foreach ($_SESSION["shopping_cart"] as $product)
                                        {
                                    ?>
                                            <tr>
                                            <form method='post' enctype="multipart/form-data" >
                                                <td>
                                                    <img src='admin-098/views/images/<?php echo $product["product_image"];?>' width="160" height="100" />
                                                </td>
                                                <td><input type='text' name='product_name' readonly='readonly' value='<?php echo $product["product_name"]; ?>' /><br /> </td>

                                                <td>
                                                        <input type='hidden' name='product_id' value="<?php echo $product["product_id"]; ?>" />
                                                        <input type="hidden" name="prices" value='.$row["product_price"].' />
                                                        <input type='hidden' name='action' value="change" />
                                                        <select name='product_quantity' class='quantity' onChange="this.form.submit()">
   
                                                            <option <?php if($product["product_quantity"]==1) echo "selected";?> value="1">1
                                                            </option>
                                                            <option <?php if($product["product_quantity"]==2) echo "selected";?> value="2">2
                                                            </option>
                                                            <option <?php if($product["product_quantity"]==3) echo "selected";?> value="3">3
                                                            </option>
                                                            <option <?php if($product["product_quantity"]==4) echo "selected";?> value="4">4
                                                            </option>
                                                            <option <?php if($product["product_quantity"]==5) echo "selected";?> value="5">5
                                                            </option>
                                                        </select>
                                                </td>
                                                </form>
                                                <td><input type='text' name='actual_price' value="<?php echo "Rs.".$product["product_price"]; ?>" readonly="readonly"/></td>
                                                <td><input type='text' name="total_price" value="<?php echo "Rs.".$product["product_price"]*$product["product_quantity"]; ?>" readonly="readonly"/></td>
                                                <td colspan="6">
                                                       <form method="post">
                                                            <input type='hidden' name='pid' value="<?php echo $product["product_id"]; ?>" />
                                                            <input type='hidden' name='action' value="remove" />
                                                            <button type='submit' name='remove' class='btn btn-primary' style="margin: 0px; padding: 15px">Remove Item</button>
                                                       </form>
                                                    </td>
                                            </tr>
                                            <?php
    
                                                $total_price += ($product["product_price"]*$product["product_quantity"]);
                                                $_SESSION["total_price"] = $total_price;
                                        }
                                        ?>  
                                    <tr>
                                        <td colspan="3">
                                            <a href="products.php" class="btn btn-block btn-info-outline display-4"><i class="fas fa-undo-alt"></i> &nbsp;  Continue Shopping </a>                                  
                                        </td>
                                        <td colspan="2">
                                            <strong>TOTAL: <input type='text' name='total_price' value="<?php echo 'Rs.'.$_SESSION["total_price"];?>" readonly="readonly"/></strong>
                                        </td>
                                        <td colspan="6">
                                        <form method="POST">
                                            <?php
                                            if(!empty($_SESSION['id'])){
                                              echo'<input type="hidden" name="action" value="checkout" />';
                                                echo'<button type="submit" name="checkout" class="btn btn-primary" style="margin: 0px; padding: 25px">Checkout</button>';
                                              }else{
                                                echo'<div>';
                                                echo' <a href="login.php" style="color: white">LOGIN</a>';
                                                echo'</div>';
                                              }  ?>
                                        </form>
                                        </td>
 
                                    </tr>
                                </tbody>
                                
                            </table>
                            
                        <?php
                        }
                        else
                        {
                            echo "<h3>Your cart is empty....!</h3>";   
                        }
                    ?>
                      
                </div>
                &nbsp;
                    <p></p>
                    &nbsp;
                    <p></p>
                    &nbsp;
                    <p></p>
                    &nbsp;
                    <p></p>
                 
    </section>
       