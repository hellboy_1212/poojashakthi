<hr>
<h1><p><b style="color:green" class="text-align">SELLER PRODUCT</b></p></h1>
<p class="text-align">Choose your Product</p>
<div class="row">
		<?php 
			$result = mysqli_query($conn,"SELECT * FROM sellers_products where status= 'Approved' ");
			while($row = mysqli_fetch_assoc($result)){
			echo '
			<div class="col-md-4 col-lg-3 col-xs-12">
				<form method="post" enctype="multipart/form-data">
					<input type="hidden" id="product_id" name="product_id" value='.$row["id"].' /> 
					<input type="hidden" name="prices" value='.$row["Exact_price"].' /> 
						<div class="product-grid4">
							<div class="product-image4">
								<img class="img-thumbnail" name = "ProductImage" src="./admin-098/views/images/'.$row["image"].'" height="250" width="200">
								<span class="product-new-label">New</span>
								<span class="product-discount-label">-10%</span>
							</div>
							<div class="product-content border">
								<h3 class="title">'.$row["name"].'</h3>
								<div class="price">
								Rs.	'.$row["Exact_price"].'
									<span>Rs. '.$row["Actual_price_show"].'</span>
								</div>
								<p data-toggle="tooltip" title="View Details" style="display: inline-block">
								<button type="button" class="btn btn-primary btn-xs view_data" id="'.$row["id"].'"  btn-sm" name="detail" ><h5 style="font-size: 12px"><i class="fas fa-eye"></i></h5></button></p>
								';
								
								if(!empty($_SESSION['id'])){ 
									echo '
								<button type="submit" name="addToCart" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Add to Cart"><span style="margin: 0" class="mbrib-cart-add mbr-iconfont mbr-iconfont-btn"></span></button>
								';}else{ 
									echo '';
								} 
						echo'
							</div>
					</div>
				</form>
			</div>'; 
		echo '<div class="modal fade" id="prod1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
				<div class="modal-dialog" role="document">
				<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Product Description</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" id="product_detail" style="overflow: hidden">
	
				</div>
				<div class="modal-footer" style="padding: 0;">
					<button type="button" class="btn btn-info btn-sm" data-dismiss="modal" data-toggle="tooltip" title="Close"><i class="fas fa-times-circle"></i></button>
				</div>				
			</div>
		</div>
	</div>';
		;}
		?>
	</div>

<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
<script>  
 $(document).ready(function(){  
      $('.view_data').click(function(){  
           var id = $(this).attr("id");  
		   console.log(id);
           $.ajax({  
                url:"views/products_details.php",  
                method:"post",  
                data:{id:id},  
                success:function(data){  
                     $('#product_detail').html(data);  
                     $('#prod1').modal("show");  
                }  
           });  
      });  
 });  
 </script>