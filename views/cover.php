<!-- cover page for home -->
<section class="header8 cid-rraSXbDuWt mbr-parallax-background" id="header8-1">
		<div class="mbr-overlay" style="opacity: 0.5; background-color: rgb(0, 0, 0);">
		</div>
		<div class="container align-center">
			<div class="row justify-content-md-center">
				<div class="mbr-white col-md-10">
					<h1 class="mbr-section-title align-center py-2 mbr-bold mbr-fonts-style display-2">Poojashakthi</h1>
					<p class="mbr-text align-center py-2 mbr-fonts-style display-5">
						Buy Pooja products from us.</p>
				</div>
			</div>
		</div>
</section>

<!--code for modal pop for homepage -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-body" style="overflow: hidden">
						<div class="row">
							<div class="col">
								<h3>Welcome to <span style="color: red;">poojashakthi.com</span> we're happy to announce that you can buy all your pooja items online.</h3>
								<p></p>
							</div>
						</div>
					</div>
					<div class="modal-footer" style="padding: 0;">
						<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal" data-toggle="tooltip" title="Close"><i class="fas fa-times-circle"></i></button>
					</div>
				</div>
			</div>
		</div>
