
	<section class="cid-rrbfBCdacw" id="footer1-3">
		<div class="container">
			<div class="media-container-row content text-white">
				<div class="col-12 col-md-4 text-center">
					<div class="media-wrap">
						<a href="">
							<img src="assets/images/logo2.png" alt="Mobirise">
						</a>
					</div>
				</div>
				<div class="col-12 col-md-3 mbr-fonts-style display-4">
					<h5 class="pb-3">
						Address
					</h5>
					<p class="mbr-text">
						1234 Street Name
						<br>City, AA 99999
					</p>
				</div>
				<div class="col-12 col-md-3 mbr-fonts-style display-4">
					<h5 class="pb-3">
						Contacts
					</h5>
					<p class="mbr-text">
						Email: support@mobirise.com
						<br>Phone: +1 (0) 000 0000 001
						<br>Fax: +1 (0) 000 0000 002
					</p>
				</div>
				<div class="col-12 col-md-2 mbr-fonts-style display-7">
					<h5 class="pb-3">
						Links
					</h5>
					<p class="mbr-text"><a class="text-primary" href="index.php">Home</a><br>
						<a class="text-primary" href="about-us.php">About Us</a><br>
						<a class="text-primary" href="products.php">Our Products</a><br>
						<a class="text-primary" href="contact-us.php">Contact Us</a>
					</p>
				</div>
			</div>
			<div class="footer-lower">
				<div class="media-container-row">
					<div class="col-sm-12">
						<hr>
					</div>
				</div>
				<div class="media-container-row mbr-white">
					<div class="col-sm-6 copyright">
						<p class="mbr-text mbr-fonts-style display-4">
							© Copyright 2019 Poojashakthi.com- All Rights Reserved
						</p>
					</div>
					<div class="col-md-6">
						<div class="social-list align-right">
							<div class="soc-item">
								<a href="https://twitter.com/" target="_blank">
									<span class="mbr-iconfont mbr-iconfont-social socicon-twitter socicon"></span>
								</a>
							</div>
							<div class="soc-item">
								<a href="https://www.facebook.com/" target="_blank">
									<span class="mbr-iconfont mbr-iconfont-social socicon-facebook socicon"></span>
								</a>
							</div>
							<div class="soc-item">
								<a href="https://www.youtube.com/" target="_blank">
									<span class="socicon-youtube socicon mbr-iconfont mbr-iconfont-social"></span>
								</a>
							</div>
							<div class="soc-item">
								<a href="https://instagram.com/" target="_blank">
									<span class="socicon-instagram socicon mbr-iconfont mbr-iconfont-social"></span>
								</a>
							</div>
							<div class="soc-item">
								<a href="https://pinterest.com/" target="_blank">
									<i class="fab fa-pinterest socicon mbr-iconfont mbr-iconfont-social"></i>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<script src="views/javascript/myscript.js"></script>
	<script src="assets/web/assets/jquery/jquery.min.js"></script>
	<script src="assets/tether/tether.min.js"></script>
	<script src="assets/popper/popper.min.js"></script>
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/parallax/jarallax.min.js"></script>
	<script src="assets/touchswipe/jquery.touch-swipe.min.js"></script>
	<script src="assets/playervimeo/vimeo_player.js"></script>
	<script src="assets/smoothscroll/smooth-scroll.js"></script>
	<script src="assets/viewportchecker/jquery.viewportchecker.js"></script>
	<script src="assets/sociallikes/social-likes.js"></script>
	<script src="assets/cookies-alert-plugin/cookies-alert-core.js"></script>
	<script src="assets/cookies-alert-plugin/cookies-alert-script.js"></script>
	<script src="assets/dropdown/js/script.min.js"></script>
	<script src="assets/theme/js/script.js"></script>
	<script src="assets/bootstrap/js/tooltip.js"></script>
		<script type="text/javascript">
		$(window).on('load', function() {
			$('#myModal').modal('show');
		});
	</script>
		<script>
		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function(e) {
					$('#blah')
						.attr('src', e.target.result);
				};

				reader.readAsDataURL(input.files[0]);
			}
		}
	</script>
		<script type="text/javascript">
		function myFunction() {
			var x = document.getElementById("myInput");
			if (x.type === "password") {
				x.type = "text";
			} else {
				x.type = "password";
			}
		}
	</script>

	<div id="scrollToTop" class="scrollToTop mbr-arrow-up"><a style="text-align: center;"><i class="mbr-arrow-up-icon mbr-arrow-up-icon-cm cm-icon cm-icon-smallarrow-up"></i></a></div>
	<input name="cookieData" type="hidden" data-cookie-text="We use cookies to give you the best experience.">