
<section class="menu cid-rrbhexAll8" once="menu" id="menu1-5">
    <nav
        class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
            aria-label="Toggle navigation">
            <div class="hamburger">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </button>
        <div class="menu-logo">
            <div class="navbar-brand">

                <span class="navbar-caption-wrap"><a class="navbar-caption text-primary display-4"
                        href="index.php">POOJASHAKTHI<br></a></span>
            </div>
        </div>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav nav-dropdown" data-app-modern-menu="true">
                <li class="nav-item">
                    <a class="nav-link link text-white display-4 " href="index.php" data-toggle="tooltip"
                        title="Main Page"><span class="mobi-mbri mobi-mbri-home mbr-iconfont mbr-iconfont-btn"></span>
                        Home</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link link text-white display-4 " href="about-us.php" data-toggle="tooltip"
                        title="About Poojashakthi"><span class="mbrib-user mbr-iconfont mbr-iconfont-btn"></span>
                        About Us</a>
                </li>
                <li class="nav-item"><a class="nav-link link text-white display-4" href="./products.php"
                        data-toggle="tooltip" title="View all our Products"><span
                            class="mbrib-shopping-basket mbr-iconfont mbr-iconfont-btn"></span>
                        Products</a></li>
                <li class="nav-item">
                    <a class="nav-link link text-white display-4" href="contact-us.php" data-toggle="tooltip"
                        title="Mail or Call Us?"><span
                            class="socicon socicon-mail mbr-iconfont mbr-iconfont-btn"></span>Contact Us</a></li>
                <li class="nav-item dropdown">
                <?php
							if(!empty($_SESSION["id"])){
							echo '
                                    <a class="nav-link link text-white dropdown-toggle display-4" href="profile.php"
                                    data-toggle="dropdown-submenu" aria-expanded="false"><span class="fa fa-user"> Hello '.$row['user_firstName'].'</span></a>
                                <div class="dropdown-menu">
                                    <a class="text-white dropdown-item display-4" href="profile.php"><i class="fas fa-sign-out-alt" > </i>Profile</a>
									<a class="text-white dropdown-item display-4" href="logout.php"><i class="fas fa-sign-out-alt" > </i>Logout</a> 
								</div>';
							}
							else{
                                echo '
                                <a class="nav-link link text-white dropdown-toggle display-4" href="index.php"
                                    data-toggle="dropdown-submenu" aria-expanded="false"><span class="fa fa-user"></span></a>
                                <div class="dropdown-menu">
                                <a class="text-white dropdown-item display-4" href="login.php"><i class="fas fa-sign-out-alt" ></i>Login</a>
                                <a class="text-white dropdown-item display-4" href="signup.php"><i class="fas fa-sign-out-alt" ></i>SignUp</a>
                                
                                </div>';
                            }
                        ?>
                </li>
            </ul>
            <?php 
            if(!empty($_SESSION['id'])){ 
                ?>
                    <div class="navbar-buttons mbr-section-btn">
                    <form method = "post" action = "./cart.php">
                        <button class="btn btn-sm btn-primary display-4" data-toggle="tooltip" name = "viewCart"
                                title="View your Cart" id = "viewCart"><span class="mbrib-shopping-cart mbr-iconfont mbr-iconfont-btn"> Cart <?php echo $cart_count ;?>
                            </span>
                        </button>
                        </form>
                    </div>
                    <!-- <div class="navbar-buttons mbr-section-btn">
                        <a class="btn btn-sm btn-primary display-4" href="./cart.php" data-toggle="tooltip"
                            title="View your Cart"><span class="mbrib-shopping-cart mbr-iconfont mbr-iconfont-btn">Cart</span>
                            </a>
                    </div> --><?php }else{ echo ' ';}?>
        </div>
    </nav>
</section>