	
<section class="header1 cid-rrgofF7kTS mbr-parallax-background" id="header16-d">
<div class="mbr-overlay" style="opacity: 0.4; background-color: rgb(0, 0, 0);">
</div>

<div class="container">
    <div class="row justify-content-md-center">
        <div class="col-md-10 align-center">
            <h1 class="mbr-section-title mbr-bold pb-3 mbr-fonts-style display-2">LOGIN TO YOUR ACCOUNT</h1>

            <p class="mbr-text pb-3 mbr-fonts-style display-7">
                Full width intro with adjustable height, background image and a color overlay. Click any text to edit or style it.
            </p>

        </div>
    </div>
</div>

</section>

<section class="mbr-section" style="background-image: linear-gradient(to right top, #e60000, #ee2721, #f53d38, #fb504d, #ff6161);">
		<div class="container">
			<div class="row">
				<div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
					<div class="card card-signin my-5">
						<div class="card-body">
							<h2 class="mbr-section-title align-center pb-3 mbr-fonts-style display-2">
								LOGIN</h2>
							
							<form class="form-signin" method="post" action="./controller/loginuser.php">
								<div class="form-label-group">
									<input type="email" class="form-control" name="user_email" placeholder="Email address" required autofocus>
								</div>

								<div class="form-label-group">
									<input type="password" class="form-control" name="user_pass" placeholder="Password" required>
								</div>

								<div class="custom-control custom-checkbox mb-3">
									<input type="checkbox" class="custom-control-input" id="customCheck1">
								</div>
								<input class="btn btn-lg btn-primary btn-block text-uppercase" name="submit" type="submit" value="sign in" >	
								<div>
                                <p>Don't Have an Account? 
                                <a  href="signup.php">Sign Up</a></p>
                                </div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
