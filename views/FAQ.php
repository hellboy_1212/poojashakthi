<section class="accordion2 cid-rrbODdMklL" id="accordion2-f">
		<div class="container">
			<div class="media-container-row pt-5">
				<div class="accordion-content">
					<h2 class="mbr-section-title align-center pb-3 mbr-fonts-style display-2">
						FAQ's</h2>
					<h3 class="mbr-section-subtitle pt-2 align-center mbr-light mbr-fonts-style display-5">
						Here are some frequently asked questions from our customers</h3>
					<div id="bootstrap-accordion_15" class="panel-group accordionStyles accordion pt-5 mt-3" role="tablist" aria-multiselectable="true">
						<div class="card">
							<div class="card-header" role="tab" id="headingOne">
								<a role="button" class="collapsed panel-title text-black" data-toggle="collapse" data-core="" href="#collapse1_15" aria-expanded="false" aria-controls="collapse1">
									<h4 class="mbr-fonts-style display-7">
										<span class="sign mbr-iconfont mbri-arrow-down inactive"></span>&nbsp;What are the products?
									</h4>
								</a>
							</div>
							<div id="collapse1_15" class="panel-collapse noScroll collapse" role="tabpanel" aria-labelledby="headingOne" data-parent="#bootstrap-accordion_15">
								<div class="panel-body p-4">
									<p class="mbr-fonts-style panel-text display-7">
										Mobirise is an offline app for Window and Mac to easily create small/medium websites, landing pages, online resumes and portfolios, promo sites for apps, events, services and products.</p>
								</div>
							</div>
						</div>

						<div class="card">
							<div class="card-header" role="tab" id="headingTwo">
								<a role="button" class="collapsed panel-title text-black" data-toggle="collapse" data-core="" href="#collapse2_15" aria-expanded="false" aria-controls="collapse2">
									<h4 class="mbr-fonts-style mbr-fonts-style display-7">
										<span class="sign mbr-iconfont mbri-arrow-down inactive"></span>&nbsp;Validity of products</h4>
								</a>

							</div>
							<div id="collapse2_15" class="panel-collapse noScroll collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#bootstrap-accordion_15">
								<div class="panel-body p-4">
									<p class="mbr-fonts-style panel-text display-7">
										Mobirise is perfect for non-techies who are not familiar with the intricacies of web development and for designers who prefer to work as visually as possible, without fighting with code. Also great for pro-coders for fast prototyping and small customers' projects.</p>
								</div>
							</div>
						</div>

						<div class="card">
							<div class="card-header" role="tab" id="headingThree">
								<a role="button" class="collapsed panel-title text-black" data-toggle="collapse" data-core="" href="#collapse3_15" aria-expanded="false" aria-controls="collapse3">
									<h4 class="mbr-fonts-style display-7">
										<span class="sign mbr-iconfont mbri-arrow-down inactive"></span> Why Poojashakthi.com?
									</h4>
								</a>
							</div>
							<div id="collapse3_15" class="panel-collapse noScroll collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#bootstrap-accordion_15">
								<div class="panel-body p-4">
									<p class="mbr-fonts-style panel-text display-7">
										Key differences from traditional builders:<br>* Minimalistic, extremely <strong>easy-to-use</strong> interface<br>* <strong>Mobile</strong>-friendliness, latest website blocks and techniques "out-the-box"<br>* <strong>Free</strong> for commercial and non-profit use</p>
								</div>
							</div>
						</div>

						<div class="card">
							<div class="card-header" role="tab" id="headingFour">
								<a role="button" class="collapsed panel-title text-black" data-toggle="collapse" data-core="" href="#collapse4_15" aria-expanded="false" aria-controls="collapse4">
									<h4 class="mbr-fonts-style display-7">
										<span class="sign mbr-iconfont mbri-arrow-down inactive"></span> This is a accordion Title 4
									</h4>
								</a>
							</div>
							<div id="collapse4_15" class="panel-collapse noScroll collapse" role="tabpanel" aria-labelledby="headingFour" data-parent="#bootstrap-accordion_15">
								<div class="panel-body p-4">
									<p class="mbr-fonts-style panel-text display-7">
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur in nulla ut magna vehicula libero luctus in ipsum consequat faucibus. Ut porta nulla ac dapibus convallis. Curabitur sit amet massa quam. In ut ex auctor, porta neque quis, sagittis purus. Nunc auctor gravida magna, sed efficitur tortor tristique quis.</p>
								</div>
							</div>
						</div>

						<div class="card">
							<div class="card-header" role="tab" id="headingFive">
								<a role="button" class="collapsed panel-title text-black" data-toggle="collapse" data-core="" href="#collapse5_15" aria-expanded="false" aria-controls="collapse5">
									<h4 class="mbr-fonts-style display-7">
										<span class="sign mbr-iconfont mbri-arrow-down inactive"></span> This is a accordion Title 5
									</h4>
								</a>
							</div>
							<div id="collapse5_15" class="panel-collapse noScroll collapse" role="tabpanel" aria-labelledby="headingFive" data-parent="#bootstrap-accordion_15">
								<div class="panel-body p-4">
									<p class="mbr-fonts-style panel-text display-7">
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur in nulla ut magna vehicula libero luctus in ipsum consequat faucibus. Ut porta nulla ac dapibus convallis. Curabitur sit amet massa quam. In ut ex auctor, porta neque quis, sagittis purus. Nunc auctor gravida magna, sed efficitur tortor tristique quis.</p>
								</div>
							</div>
						</div>

						<div class="card">
							<div class="card-header" role="tab" id="headingSix">
								<a role="button" class="collapsed panel-title text-black" data-toggle="collapse" data-core="" href="#collapse6_15" aria-expanded="false" aria-controls="collapse6">
									<h4 class="mbr-fonts-style display-7">
										<span class="sign mbr-iconfont mbri-arrow-down inactive"></span> This is a accordion Title 6
									</h4>
								</a>
							</div>
							<div id="collapse6_15" class="panel-collapse noScroll collapse" role="tabpanel" aria-labelledby="headingSix" data-parent="#bootstrap-accordion_15">
								<div class="panel-body p-4">
									<p class="mbr-fonts-style panel-text display-7">
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur in nulla ut magna vehicula libero luctus in ipsum consequat faucibus. Ut porta nulla ac dapibus convallis. Curabitur sit amet massa quam. In ut ex auctor, porta neque quis, sagittis purus. Nunc auctor gravida magna, sed efficitur tortor tristique quis.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="mbr-figure" style="width: 92%;">
					<img src="assets/images/background8.jpg" alt="Mobirise">
				</div>
			</div>
		</div>
	</section>