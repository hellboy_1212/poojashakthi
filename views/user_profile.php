&nbsp;
<p></p>
&nbsp;
<p></p>


	
<section class="mbr-section" style="background-image: linear-gradient(to right top, #e60000, #ee2721, #f53d38, #fb504d, #ff6161);">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-5">
					<div class="card card-signin flex-row my-5">
						<div class="card-img-left d-none d-md-flex">
						</div>
						<div class="card-body">
							<h4 class="mbr-section-title align-center pb-3 mbr-fonts-style display-5">
								PROFILE</h4>
								<hr>
							<div class="user-profile text-center">
								<img src="image.jpg" alt="">
							</div>
							<div class="py-5">

							<form method="post">
								<table class="table" >
									<tbody>
										<tr>
											<th>FULL NAME</th>
											<td>
												<?php 
													if(!empty($_SESSION["id"])){
														echo ''.$row["user_firstName"].$row["user_lastName"].'';
													}else{
														echo ' empty ';
													}
												?>
											</td>
										</tr>

										<tr>
											<th>EMAIL</th>
											<td>
												<?php
													if(!empty($_SESSION["id"])){
														echo ''.$row["user_email"].'' ;
													}else{
														echo 'empty ';
													}
												?>
											</td>
										</tr>
										<tr>
											<th>PHONE NUMBER</th>
											<td>
												<?php
													if(!empty($_SESSION["id"])){
														 echo ''.$row["user_phone"].'' ;
													}else{
														echo ' empty ';
													}
												?>
											</td>
										</tr>
										<tr>
											<th>SHIPPING ADDRESS</th>
											<td>
												<?php 
													if(!empty($_SESSION["id"])){
														 echo ''.$row["user_address"].'' ;
													}else{
														echo ' empty ';
													}
												?>
											</td>
										</tr>
										<tr>
											<th>BILLING ADDRESS</th>
											<td>
												<?php 
													if(!empty($_SESSION["id"])){
														 echo ''.$row["user_address"].'' ;
													}else{
														echo ' empty ';
													}
												?>
											</td>
										</tr>
									</tbody>
								</table>
								<tr>
									<td>
										<?php 
											if(!empty($_SESSION["id"])){
												echo "<b><a href='update.php?update={$row['user_id']}' class='btn btn-primary'>CHANGE DETAILS</a></b>";
											}else{
												echo '<a href="login.php" class="btn btn-primary ">Login</a>';
											}
										?> 
										</td>
								</tr>
								
							</form>
					
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</section>
