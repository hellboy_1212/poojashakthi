<main class="page payment-page" style="margin-top: 50px;">
		<section class="payment-form dark" style="background-image: linear-gradient(to right top, #e60000, #ee2721, #f53d38, #fb504d, #ff6161);">
			<div class="container">
				<div >
					<p style="height: 20px"></p>
				</div>
				<div >
					<p style="height: 20px"></p>
				</div>
				<div >
					<p style="height: 20px"></p>
				</div>
				<div class="block-heading" style="text-align: center">
						<h2 class="pb-3 mbr-section-title mbr-fonts-style text-white" style="font-size: 40px;"><strong>
                    CHECK OUT</strong></h2>
		          <p class="text-white">PAY YOUR BILL.</p>
				</div>
				<table class="table">
					<tr>
						<th>Product Name</th>
						<th>Product Per Price</th>
						<th>Product Quantity</th>
					</tr>
				<?php
					if(isset($_SESSION["shopping_cart"])){	
						echo '
							<div>
								<form method="post">';
									foreach ($_SESSION["shopping_cart"] as $product)
									{
										echo '
											<input type="hidden" readonly="readonly" name="product_id" value="'.$product['product_id'].'" >
											<tr>
												<td><input type="text" readonly="readonly" name="productname" value='.$product['product_name'].'></td>  
												<td>Rs.<input type="text" readonly="readonly" name="price" value='.$product['product_price'].' ></td> 
												<td><input type="text" readonly="readonly" name="price" value='.$product['product_quantity'].'></td>
												
											</tr>';	
									}
										echo'<tr>
										<td></td><td><h4><b>TOTAL PRICE</b></h4></td>
										<td><input type="text" readonly="readonly" name="totalprice" value='.$_SESSION["total_price"].'></td><tr>';
										echo'<tr><td><a href="cart.php" style="color: white" class="btn btn-primary" > BACK </a></td>	
										<td></td><td><a href="https://www.paypal.com/webapps/shoppingcart?flowlogging_id=6dc787ef2c9d0&mfid=1561870664467_6dc787ef2c9d0#/checkout/openButton" class="btn btn-primary">Pay Now</a></td><tr>';
										echo '</form>
								</div>';
							}
						?>
					</table>		
				</div>
		</section>
		</main>