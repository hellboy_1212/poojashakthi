<section class="mbr-section"
        style="background-image: linear-gradient(to right top, #ee2721, #f53d38,);">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-xl-9 mx-auto">
                    <div class="card card-signin my-5">
                        <div class="card-img-left d-none d-md-flex">
                        </div>
                        <div class="card-body">
                            <h2 class=" align-center ">
                                PLEASE ENTER YOUR VALUABLE COMMENT HERE</h2><br />
                            <form action="controller/comment.php" class="form-signin"  method="post" >
                                <div class="form-label-group">
                                <label>NAME:</label> <br />
                                    <input type="text" name="user_name" class="form-control"
                                        placeholder="Name" minlength="3" maxlength="12"  required>
                                </div>

                                <div class="form-label-group">
                                <label>COMMENT:</label> <br />
                                    <input type="text" name="user_comment" class="form-control"
                                        placeholder="Your Comment about poojashakthi" required ><br />
                                <?php
                                    if(!empty($_SESSION['id'])){
                                        echo'
                                        <input type="submit" value="Post" name="submit" class="btn btn-primary btn-block " >';
                                    }else{
                                        echo'
                                        <a href="login.php" class="btn btn-primary btn-block " >Login for comment</a>';
                                    }
                                ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>