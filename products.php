<?php
session_start();
include('model/database.php');
include("controller/cartController.php");
include('cart_count.php');
?>

<!DOCTYPE html>
<html>
<?php 
include('views/template/header.php'); ?>
<body>
<?php
include("controller/userProfileController.php");
include('views/template/nav.php');
include('views/product_cover.php');
include('views/product_header.php');
?>
<h3 style="background-color: white;"><b style="padding: 10px;"><?php echo $status; ?></b></h3>
<?php
include('views/product_section.php');   
include('views/product_main.php');
include('views/template/footer.php');
?>
</body>
</html>