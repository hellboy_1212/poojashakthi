<?php
session_start();
include('model/database.php');
include("controller/cartController.php");
include('cart_count.php');
?>
<!DOCTYPE html>
<html>
<?php 
include('views/template/header.php');
 
   ?>

<body>
    <?php 
    if(!empty($_SESSION["id"])) {
        include("controller/userProfileController.php");
        include('views/template/nav.php');
        include('views/cart_section.php');
        include('views/template/footer.php');
    }
    else 
    {
        header('location:login.php');
    }
    ?>

</body>

</html>