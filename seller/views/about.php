<section class="counters4 counters cid-rrbRVHcPZe" id="counters4-h">
		<div class="container pt-4 mt-2">
			<h2 class="mbr-section-title pb-3 align-center mbr-fonts-style display-2">About Us</h2>
			<h3 class="mbr-section-subtitle pb-5 align-center mbr-fonts-style display-5">
				Responsive block with counters and media.
			</h3>
			<div class="media-container-row">
				<div class="media-block m-auto" style="width: 49%;">
					<div class="mbr-figure">
						<img src="assets/images/background6.jpg">
					</div>
				</div>
				<div class="cards-block">
					<div class="cards-container">
						<div class="card px-3 align-left col-12">
							<div class="panel-item p-4 d-flex align-items-start">
								<div class="card-img pr-3">
									<h3 class="img-text d-flex align-items-center justify-content-center">
										1
									</h3>
								</div>
								<div class="card-text">
									<h4 class="mbr-content-title mbr-bold mbr-fonts-style display-7">Honest and Dependable</h4>
									<p class="mbr-content-text mbr-fonts-style display-7">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae cupiditate rerum ipsum tempora vero.</p>
								</div>
							</div>
						</div>
						<div class="card px-3 align-left col-12">
							<div class="panel-item p-4 d-flex align-items-start">
								<div class="card-img pr-3">
									<h3 class="img-text d-flex align-items-center justify-content-center">
										2
									</h3>
								</div>
								<div class="card-text">
									<h4 class="mbr-content-title mbr-bold mbr-fonts-style display-7">We Are Always Improving</h4>
									<p class="mbr-content-text mbr-fonts-style display-7">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit.
									</p>
								</div>
							</div>
						</div>
						<div class="card px-3 align-left col-12">
							<div class="panel-item p-4 d-flex align-items-start">
								<div class="card-img pr-3">
									<h3 class="img-text d-flex align-items-center justify-content-center">
										3
									</h3>
								</div>
								<div class="card-text">
									<h4 class="mbr-content-title mbr-bold mbr-fonts-style display-7">We Are Passionate</h4>
									<p class="mbr-content-text mbr-fonts-style display-7">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae cupiditate rerum ipsum tempora vero.</p>
								</div>
							</div>
						</div>
						<div class="card px-3 align-left col-12">
							<div class="panel-item p-4 d-flex align-items-start">
								<div class="card-img pr-3">
									<h3 class="img-text d-flex align-items-center justify-content-center">
										4
									</h3>
								</div>
								<div class="card-texts">
									<h4 class="mbr-content-title mbr-bold mbr-fonts-style display-7">
										We Are Passionate
									</h4>
									<p class="mbr-content-text mbr-fonts-style display-7">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae cupiditate rerum ipsum tempora vero.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>