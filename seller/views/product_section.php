
<div class="row">
		<?php 
		if(!empty($_SESSION['id'])){
			$seller_id = $_SESSION['id'];
			$result = mysqli_query($conn,"SELECT * FROM sellers_products where  sid = $seller_id ");
			while($row = mysqli_fetch_assoc($result)){
			echo '
			<div class="col-md-4 col-lg-3 col-xs-12">
				<form method="post" enctype="multipart/form-data">
					<input type="hidden" id="id" name="id" value='.$row["id"].' /> 
					<input type="hidden" name="prices" value='.$row["price"].' /> 
						<div class="product-grid4">
							<div class="product-image4">
								<img class="img-thumbnail" name = "ProductImage" src="../admin-098/views/images/'.$row["image"].'" height="250" width="200">
							</div>
							<div class="product-content border">
								<h3 class="title">'.$row["name"].'</h3>
								<div class="price">
								Rs.	'.$row["price"].'
								</div>
								<p data-toggle="tooltip" title="View Details" style="display: inline-block">
								<button type="button" class="btn btn-primary btn-xs view_data" id="'.$row["id"].'"  btn-sm" name="detail" >DETAIL</button></p>
								'; 
						echo'
							</div>
					</div>
				</form>
			</div>'; 
		echo '<div class="modal fade" id="prod1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
				<div class="modal-dialog" role="document">
				<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Product Description</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" id="product_detail" style="overflow: hidden">
	
				</div>				
			</div>
		</div>
	</div>';
		;}
	}
		?>
	</div>

<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
<script>  
 $(document).ready(function(){  
      $('.view_data').click(function(){  
           var id = $(this).attr("id");  
		   console.log(id);
           $.ajax({  
                url:"views/product_details.php",  
                method:"post",  
                data:{id:id},  
                success:function(data){  
                     $('#product_detail').html(data);  
                     $('#prod1').modal("show");  
                }  
           });  
      });  
 });  
 </script>