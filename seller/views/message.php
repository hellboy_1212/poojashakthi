<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="google-map"><iframe frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyAEIpgj38KyLFELm2bK9Y7krBkz1K-cMq8&amp;q=place_id:ChIJn6wOs6lZwokRLKy1iqRcoKw" allowfullscreen=""></iframe></div>
				</div>
				<div class="col-md-6">

					<div>
						<div class="icon-block pb-3 align-left">


						</div>
						<div class="icon-contacts pb-3">

							<p class="mbr-text align-left mbr-fonts-style display-7"><strong>
									Phone</strong>: +91 00000 00000<br><strong>
									Email</strong>: Poojashakthi.query@mail.com
							</p>
						</div>
					</div>
					<div data-form-type="formoid">
						<!---Formbuilder Form--->
						<form action="controller/contactControl.php" method="POST" class="mbr-form form-with-styler">
							<div class="row">
								<div hidden="hidden" data-form-alert-danger="" class="alert alert-danger col-12"></div>
							</div>

							<div class="dragArea row">
								<div class="col-md-6  form-group" data-for="name" action="contactControl.php">
									<input type="text" name="name" placeholder="Name" data-form-field="Name" required="required" class="form-control input display-7" id="name-form4-j">
								</div>

								<div class="col-md-6  form-group" data-for="subject">
									<input type="text" name="subject" placeholder="subject" data-form-field="subject" required="required" class="form-control input display-7" id="phone-form4-j">
								</div>

								<div data-for="email" class="col-md-12  form-group">
									<input type="email" name="email" placeholder="Email" data-form-field="Email" class="form-control input display-7" required="required" id="email-form4-j">
								</div>

								<div data-for="message" class="col-md-12  form-group">
									<textarea name="message" placeholder="Message" data-form-field="Message" class="form-control input display-7" id="message-form4-j" required="required"></textarea>
								</div>
						<?php if(!empty($_SESSION['id'])){?>
								<div class="col-md-12 input-group-btn  mt-2 align-center">
                                    <button type="submit" name="submit" class="btn btn-primary btn-form display-4">SEND MESSAGE</button>
								</div>
								<?php }else{ ?>
								<div class="col-md-12 input-group-btn  mt-2 align-center">
                                    <a href="login-as-seller" class="btn btn-primary">LOGIN TO SEND MESSAGE</a>
								</div>
								<?php } ?>
							</div>
						</form>
                      
						<!---Formbuilder Form--->
					</div>
				</div>
			</div>
		</div>
	</section>