<section class="header1 cid-rrgofF7kTS mbr-parallax-background" id="header16-d">
		<div class="mbr-overlay" style="opacity: 0.4; background-color: rgb(0, 0, 0);">
		</div>
		<div class="container">
			<div class="row justify-content-md-center">
				<div class="col-md-10 align-center">
					<h1 class="mbr-section-title mbr-bold pb-3 mbr-fonts-style display-2" style="color:white">See Your Uploaded Products</h1>

					<p class="mbr-text pb-3 mbr-fonts-style display-7">
						Buy some new products available in our site
					</p>

				</div>
			</div>
		</div>
</section>