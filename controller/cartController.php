<?php
    include("./model/database.php");
    
    $status="";
    $userId = '';
     
   //cart count
  


    if(isset($_SESSION['id'])){
        $userId = $_SESSION['id'];
    }
    
    if (isset($_POST['viewCart']))
    {
        if(empty($_SESSION["id"])) {
            header('location:login.php');
            // echo '<script> location.href = "login.php"> </script>';
        }
        else 
        {
            // unset($_SESSION["shopping_cart"]);
            $query = "SELECT * FROM products, addtocart WHERE addtocart.user_id='$userId' and products.product_id = addtocart.product_id";
            $result = mysqli_query($conn,$query);
            if (!$result){
                echo "DataBase error";
            }
            while($rows = mysqli_fetch_assoc($result))
            {
                $product_name = $rows['product_name'];
                $product_id = $rows['product_id'];
                $product_price = $rows['product_price'];
                $product_image = $rows['product_image'];
                
                $cartArray[] = array(
                    'product_name'=>$product_name,
                    'product_id'=>$product_id,
                    'product_price'=>$product_price,
                    'product_quantity'=>1,
                    'product_image'=>$product_image);
                

                $_SESSION["shopping_cart"] = $cartArray;
                
               //$_SESSION["shopping_cart"] = array_merge($_SESSION["shopping_cart"],$cartArray);
            }
        }
    }
            
        
    
    
    //add to cart function
    
      if (isset($_POST['addToCart']))
        {
            if(empty($_SESSION["id"])) {
                header('location:login.php');
                // echo '<script> location.href = "login.php"> </script>';
            }
            else 
            {
                $userId = $_SESSION['id'];
                $productId = mysqli_real_escape_string($conn, $_POST['product_id']);
                $query1 = "SELECT * from addtocart where product_id = '$productId' AND user_id ='$userId'";
            
                $res1 = mysqli_query($conn, $query1);
                if(!$res1){
                    die('Error: ' . mysqli_error($conn));
                }
                if (mysqli_num_rows($res1) >= 1) {
                    mysqli_close($conn);
                    $status = "<div class='box' style='color:red;'>
                    Product is already added to your cart!</div>";
                    
                } 

                else 
                {
                    $product_price = mysqli_real_escape_string($conn, $_POST['prices']);
                    // $quer3 = "SELECT product_price from products where product_id = $productId";
                    // $result2  = mysqli_query($conn, $query3);
                    // $product_price = $result2['product_price'];
                    $query2 = "INSERT INTO addtocart (user_id,product_id,product_price) VALUES ('$userId','$productId','$product_price')";
                    $results  = mysqli_query($conn, $query2);
                    if(!$results){
                        die('Error: ' . mysqli_error($conn));
                    }
                    else {
                    mysqli_close($conn);
                    $status = "<div class='box'>Product is added to your cart!</div>";
                    }
                    
                }
                // echo '<script> location.href = "login.php"> </script>';
            }
            
        }
        
    

    // if (isset($_POST['action']) && $_POST['action']=="remove")
    // {
    //     if(!empty($_SESSION["shopping_cart"])) 
    //     {
    //         foreach($_SESSION["shopping_cart"] as $key => $value) 
    //         {  
    //             if($_POST["product_id"] == $key)
    //             {
    //                 unset($_SESSION["shopping_cart"][$key]);
    //                 $query2 = "Delete from addToCart where product_id = '$key'";
    //                 $results  = mysqli_query($conn, $query2);
    //                 if(!$results){
    //                     echo "db error";
    //                     die('Error: ' . mysqli_error($conn));
    //                 }
    //                 else {
    //                 mysqli_close($conn);
    //                 $status = "<div class='box' style='color:red;'>
    //                 Product is removed from your cart!</div>";
    //                 echo $status;
    //                 }

                    
    //             }

    //             if(empty($_SESSION["shopping_cart"]))
    //             unset($_SESSION["shopping_cart"]);
    //         }		
    //     }
    // }

    if (isset($_POST['action']) && $_POST['action']=="remove")
    {
        if(!empty($_SESSION["shopping_cart"])) 
        {
            $key = $_POST["pid"];
            $query2 = "DELETE from addtocart where product_id = '$key' and user_id = '$userId'";
            unset($_SESSION["shopping_cart"][$key]);

            $results  = mysqli_query($conn, $query2);
            if(!$results){
                die('Error: ' . mysqli_error($conn));
            }
            else {
            mysqli_close($conn);
            $status = "<div class='box' style='color:red;'>
            Product is removed from your cart!</div>";
            }
            unset($_SESSION["shopping_cart"]);
            header('location: products.php');
        }else{
                 unset($_SESSION["shopping_cart"]);
       }	
    }
    
    if (isset($_POST['action']) && $_POST['action']=="change")
    {
        foreach($_SESSION["shopping_cart"] as &$value)
        {
            if($value['product_id'] === $_POST["product_id"])
            {
                $value['product_quantity'] = $_POST["product_quantity"];
                break; // Stop the loop after we've found the product
            }
        }
        
    }

    //checkout button controller
    if(isset($_POST['action']) && $_POST['action']=="checkout"){
        header('location: checkout.php');
    }
?>