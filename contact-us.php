<?php
session_start();
include("model/database.php");

?>
<!DOCTYPE html>
<html  >
<?php 
	include('views/template/header.php');
	include('cart_count.php');
?>
<body>
<?php
	include("controller/userProfileController.php");
	include('views/template/nav.php');
	include('views/contact.php');
	include('views/message.php');
	include('views/template/footer.php'); ?>
</body>
</html>