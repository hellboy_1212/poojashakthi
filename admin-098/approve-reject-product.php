<?php 
session_start();
$url='localhost';
$username='root';
$password='';
$conn=mysqli_connect($url,$username,$password,"pshakthi");
if(!$conn){
	die('Could not Connect Mysql:' .mysqli_error());
}
?>
<!doctype html>
<html lang="en">
<head>
<?php
		include('views/template/adminheader.php');
	?>

	<title>Light Bootstrap Dashboard by Creative Tim</title>
</head>

<body>

<?php
		include('views/template/adminnav.php');
	?>

		<div class="main-panel">
			<nav class="navbar navbar-default navbar-fixed">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<span class="navbar-brand" href="#">Ordered Products</span>
					</div>
					<?php
					if(!empty($_SESSION["id"])){
							include('views/template/logout.php');
						}else{
							echo '';
						}
						if(!empty($_SESSION['id'])){
						?>
			</nav>


			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="header">
									<h4 class="title">Approve or Reject products</h4>
								</div>
								<div class="content table-responsive table-full-width">
								<form method="post" enctype="multipart/form-data" >  
								<?php
									$aa="SELECT * from sellers_products where status='pending'";
									$res=mysqli_query($conn, $aa);
									while($ar=mysqli_fetch_array($res))
									{

									?>
									<div class="container">
									<br>
									<h4><?php echo $ar[3];?></h4>
									<a href="approve-rejectnew.php ? id=<?php echo $ar[0];?> "><h6><u>Approve/ Reject</u></h6></a></div>
									<?php
									}
								?>
								</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


			<?php 
				include('views/template/adminfooter.php');
				}
			?>

		</div>
	</div>


</body>

<!--   Core JS Files   -->
<script src="dashboard/assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="dashboard/assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="dashboard/assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="dashboard/assets/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="dashboard/assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="dashboard/assets/js/demo.js"></script></html>