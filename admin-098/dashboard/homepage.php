<!doctype html>
<html lang="en">
<head>
<?php
		include('views/template/adminheader.php');
	?>
	<title>Upload Products - Home Page</title>

</head>

<body>

	<div class="wrapper">
		<div class="sidebar" data-color="purple" data-image="assets/img/sidebar-5.jpg">

			<!--   you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple" -->


			<div class="sidebar-wrapper">
				<div class="logo">
					<a href="http://www.creative-tim.com" class="simple-text">
						POOJASHAKTI
					</a>
				</div>

				<ul class="nav">
					<li>
						<a href="dashboard.php">
							<i class="pe-7s-graph"></i>
							<p>Dashboard</p>
						</a>
					</li>
					<li>
						<a href="user.php">
							<i class="pe-7s-user"></i>
							<p>User Profile</p>
						</a>
					</li>
					<li class="active">
						<a href="homepage.php">
							<i class="pe-7s-cloud-upload"></i>
							<p>Home Page</p>
						</a>
					</li>
					<li>
						<a href="productspagegeneral.php">
							<i class="pe-7s-cloud-upload"></i>
							<p>Product Page (general)</p>
						</a>
					</li>
					<li>
						<a href="productspagespecials.php">
							<i class="pe-7s-cloud-upload"></i>
							<p>Product Page (Specials)</p>
						</a>
					</li>
					
					<li>
						<a href="approve-reject.php">
							<i class="pe-7s-way"></i>
							<p>Approve / Reject</p>
						</a>
					</li>
				</ul>
			</div>
		</div>

		<div class="main-panel">
			<nav class="navbar navbar-default navbar-fixed">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#">Home Page</a>
					</div>
					<div class="collapse navbar-collapse">
						<ul class="nav navbar-nav navbar-left">
							<li>
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="fa fa-dashboard"></i>
									<p class="hidden-lg hidden-md">Dashboard</p>
								</a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="fa fa-globe"></i>
									<b class="caret hidden-sm hidden-xs"></b>
									<span class="notification hidden-sm hidden-xs">5</span>
									<p class="hidden-lg hidden-md">
										5 Notifications
										<b class="caret"></b>
									</p>
								</a>
								<ul class="dropdown-menu">
									<li><a href="#">Notification 1</a></li>
									<li><a href="#">Notification 2</a></li>
									<li><a href="#">Notification 3</a></li>
									<li><a href="#">Notification 4</a></li>
									<li><a href="#">Another notification</a></li>
								</ul>
							</li>
							<li>
								<a href="">
									<i class="fa fa-search"></i>
									<p class="hidden-lg hidden-md">Search</p>
								</a>
							</li>
						</ul>

						<ul class="nav navbar-nav navbar-right">
							<li>
								<a href="">
									<p>Account</p>
								</a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<p>
										Dropdown
										<b class="caret"></b>
									</p>

								</a>
								<ul class="dropdown-menu">
									<li><a href="#">Action</a></li>
									<li><a href="#">Another action</a></li>
									<li><a href="#">Something</a></li>
									<li><a href="#">Another action</a></li>
									<li><a href="#">Something</a></li>
									<li class="divider"></li>
									<li><a href="#">Separated link</a></li>
								</ul>
							</li>
							<li>
								<a href="#">
									<p>Log out</p>
								</a>
							</li>
							<li class="separator hidden-lg hidden-md"></li>
						</ul>
					</div>
				</div>
			</nav>


			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="header">
									<h2 class="title" style="font-weight: bold">Upload Product Details on Home Page - Special Discount</h2><br>
									<p class="category"><u>Poojashakti.com</u>- Home Page</p>
									<hr>
								</div>
								<div class="content">
									<form class="form-signin">
										<div class="form-label-group">
											<label for="sel1">Select Product Catogory:</label>
											<select class="form-control" id="sel1"  required>
												<option>Select Category</option>
												<option>Catogory1</option>
												<option>Catogory2</option>
												<option>Catogory3</option>
												<option>Catogory4</option>
												<option>Catogory5</option>
												<option>Catogory6</option>
											</select>
										</div>
										<div class="form-label-group">
											<label for="exampleFormControlTextarea1">Product Name:</label>
											<input type="text" id="text" class="form-control" required placeholder="Full name of the product">
										</div>
										<div class="form-label-group">
											<label for="exampleFormControlTextarea1">Product Price (in dollars $):</label>
											<input type="number" id="text" class="form-control" required placeholder="Price of the product in $">

										</div>
										<div class="form-label-group">
											<label for="exampleFormControlTextarea1">Product Discription (Minimum 60 words):</label>
											<textarea class="form-control" id="exampleFormControlTextarea1" rows="4" required></textarea>
										</div>

										<div class="form-label-group">
											<label for="exampleFormControlTextarea1">Upload Image:</label> <br>
											<input class="form-control" type='file' required onchange="readURL(this);" /><br><br>
											<div class="text-center">
												<img id="blah" src="assets/img/your-image.png" alt="your image" />
											</div>
										</div>
										<div class="text-center">
										<br>
											<button class="btn btn-info btn-lg btn-fill text-uppercase" type="submit"> SUBMIT </button>
										</div>
										<hr class="my-4">
									</form>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>

			<footer class="footer">
				<div class="container-fluid">
					<nav class="pull-left">
						<ul>
							<li>
								<a href="#">
									Home
								</a>
							</li>
							<li>
								<a href="#">
									Company
								</a>
							</li>
							<li>
								<a href="#">
									Portfolio
								</a>
							</li>
							<li>
								<a href="#">
									Blog
								</a>
							</li>
						</ul>
					</nav>
					<p class="copyright pull-right">
						&copy; <script>
							document.write(new Date().getFullYear())
						</script> <a href="http://www.creative-tim.com">Creative Tim</a>, made with love for a better web
					</p>
				</div>
			</footer>


		</div>
	</div>
	<script>
		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function(e) {
					$('#blah')
						.attr('src', e.target.result);
				};

				reader.readAsDataURL(input.files[0]);
			}
		}
	</script>
	
</body>

<!--   Core JS Files   -->
<script src="assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="assets/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="assets/js/demo.js"></script>
<script type="text/javascript">
    	$(document).ready(function(){

        	demo.initChartist();

        	$.notify({
            	icon: 'pe-7s-cloud-upload',
            	message: "Upload the products on <b>Home Page</b> - Poojashati.com"

            },{
                type: 'warning',
                timer: 4000
            });

    	});

	</script>


</html>
