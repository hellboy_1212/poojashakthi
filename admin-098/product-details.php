<?php
	session_start();
	
?>
<!doctype html>
<html lang="en">
<head>
<?php
		include('views/template/adminheader.php');
	?>

	<title>Light Bootstrap Dashboard by Creative Tim</title>

</head>

<body>

<?php
		include('views/template/adminnav.php');
	?>

		<div class="main-panel">
			<nav class="navbar navbar-default navbar-fixed">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<span class="navbar-brand" href="#">Products Details</span>
					</div>
					<?php 
						if(!empty($_SESSION["id"])){
							include('views/template/logout.php');
						}else{
							echo '';
						};
						if(!empty($_SESSION['id'])){
					?>
				</nav>
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="header">
									<h4 class="title">Details of sellers accounts information</h4>
								</div>
								<div class="content table-responsive table-full-width">
							
										<table class="table table-hover table-striped">

												<tr>
													<th>Sl. No.</th>
													<th>Product Name</th>
													<th>Category Name</th>
													<th>Product Image</th>
													<th>Product Description</th>
													<th>Product Price</th>
													<th>UPDATE</th>
												</tr>
										
										<?php
										
											$url='localhost';
											$username='root';
											$password='';
											$conn=mysqli_connect($url,$username,$password,"pshakthi");
											if(!$conn){
												die('Could not Connect Mysql:' .mysql_error());
											}
											$result = mysqli_query($conn," SELECT * FROM `products` ");
											while($rows = mysqli_fetch_assoc($result)){
										?>	
													<tr>
															<td><?php echo $rows['product_id']; ?></td>
															<td><?php echo $rows['product_name']; ?> </td>
															<td><?php echo $rows['category_id']; ?> </td>
															<td><?php echo $rows['product_image']; ?></td>
															<td><?php echo $rows['product_description']; ?></td>
															<td><?php echo $rows['product_price']; ?> </td>
															<td><?php echo "<b><a href='productupdate.php?update={$rows['product_id']}' class='btn btn-primary'>UPDATE PRODUCTS</a></b>";?></td>
														</tr>
												<?php
													}
												}else{
														echo '<center><a href="login-dashboard.php" class="btn btn-primary" >LOGIN</a><center>';
														echo '<img src="https://previews.123rf.com/images/cougarsan/cougarsan1709/cougarsan170900015/85028916-the-emoji-yellow-disappointed-upset-face-and-closing-eyes-icon.jpg" height="400" width="500" ';
													}
												?>
										</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


			
			<?php 
				include('views/template/adminfooter.php');
			?>


		</div>
	</div>


</body>

<!--   Core JS Files   -->
<script src="assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="assets/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="assets/js/demo.js"></script></html>
