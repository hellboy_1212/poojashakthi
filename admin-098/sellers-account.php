<?php 
	session_start();
	$msg = "";
?>
<!doctype html>
<html lang="en">
<head>
<?php
		include('views/template/adminheader.php');
	?>
	<title>Light Bootstrap Dashboard by Creative Tim</title>
	<script type="text/javascript">
    function myFunction() {
        var x = document.getElementById("myInput");
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
    }
	</script>
</head>

<body>

<?php
		include('views/template/adminnav.php');
	?>

		<div class="main-panel">
			<nav class="navbar navbar-default navbar-fixed">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<span class="navbar-brand" href="#">Sellers Account</span>
					</div>
					<?php
						if(!empty($_SESSION["id"])){
							include('views/template/logout.php');
						}else{
							echo '';
						};
						if(!empty($_SESSION['id'])){
					?>
			</nav>


			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="header">
									<h4 class="title">Details of sellers accounts information</h4>
								</div>
								<div class="content table-responsive table-full-width">
								<form method="post" action="controller/sellersignup.php" name="form1">
									<table class="table table-hover table-striped">
									
										<tr><td>First Name</td><td><input type="text" name="user_firstName" placeholder="first name" required/></td></tr>
										<tr><td>last Name</td><td><input type="text" name="user_lastName" placeholder="last name" required/></td></tr>
										<tr><td>Email</td><td><input type="email" name="user_email" placeholder="email" required/></td></tr>
										<tr><td>Phone No.</td><td><input type="text" name="user_phone" placeholder="mobile number" required/></td></tr>
										<tr><td>Address</td><td><input type="text" name="user_addres" placeholder="address" required/></td></tr>
										<tr><td>Password</td><td><input type="password" name="user_pass" id="myInput" placeholder="password" required/> <input type="checkbox" onclick="myFunction()"> Show Password</td></tr>
										<tr><td></td><td><input type="submit" name="submit" value="Add New Seller" class="btn btn-primary";/></td></tr>
			
									</table>
									
									</form>	
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php
				}else{
					echo '<center><a href="login-dashboard.php" class="btn btn-primary" >LOGIN</a><center>';
					echo '<img src="https://previews.123rf.com/images/cougarsan/cougarsan1709/cougarsan170900015/85028916-the-emoji-yellow-disappointed-upset-face-and-closing-eyes-icon.jpg" height="400" width="500" ';
				}
				?>
		</div>
	</div>


</body>

<!--   Core JS Files   -->
<script src="dashboard/assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="dashboard/assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="dashboard/assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="dashboard/assets/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="dashboard/assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="dashboard/assets/js/demo.js"></script></html>
