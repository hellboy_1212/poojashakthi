<?php
	session_start();
	$url='localhost';
	$username='root';
	$password='';
	$conn=mysqli_connect($url,$username,$password,"pshakthi");
	if(!$conn){
		die('Could not Connect Mysql:' .mysqli_error());
	}
?>
<!doctype html>
<html lang="en">
<head>
<?php
		include('views/template/adminheader.php');
	?>

	<title>Light Bootstrap Dashboard by Creative Tim</title>

	
</head>

<body>
<?php
		include('views/template/adminnav.php');
	?>
	

		<div class="main-panel">
			<nav class="navbar navbar-default navbar-fixed">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<span class="navbar-brand" href="#">Ordered Products</span>
					</div>
					<?php 
						if(!empty($_SESSION["id"])){
							include('views/template/logout.php');
						}else{
							echo '';
						};
						if(!empty($_SESSION['id'])){
					?>
				</nav>


			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="header">
									<h4 class="title">Information of ordered products</h4>
								</div>
								<div class="content table-responsive table-full-width">
									<table class="table table-hover table-striped">
										<thead>
											<tr>
												<th>Product ID</th>
												<th>Prod. Name</th>
											   <th>Prod. Image</th>
												<th>Product Price </th>
												<th>Quantity</th>

												<th>Seller ID</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<body>

													<form method="post" enctype="multipart/form-data" >  
													<?php
													$sel=mysqli_query($conn, "SELECT * from ordered_products ");
													if($a=mysqli_fetch_array($sel))

													{
													?>
													<td> <?php echo $a[1];?></td>
													<td><?php echo $a[2];?></td>
													<td><img src="admin-098/views/images/<?php echo $a[3];?>" height="200" width="200"/></td>
													<td><?php echo $a[4];?></td>
													<td><?php echo $a[5];?></td>
													<td><?php echo $a[6];?></td>

													</tr></table>
													</form>

													<?php
													}
													?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

				<?php
						}else{
				echo '<center><a href="login-dashboard.php" class="btn btn-primary" >LOGIN</a><center>';
				echo '<img src="https://previews.123rf.com/images/cougarsan/cougarsan1709/cougarsan170900015/85028916-the-emoji-yellow-disappointed-upset-face-and-closing-eyes-icon.jpg" height="400" width="500" ';
				}
			?>

		</div>
	</div>


</body>

<!--   Core JS Files   -->
<script src="dashboard/assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="dashboard/assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="dashboard/assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="dashboard/assets/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="dashboard/assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="dashboard/assets/js/demo.js"></script></html>
