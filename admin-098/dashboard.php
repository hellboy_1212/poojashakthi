<?php 
	session_start();

?>

<!doctype html>
<html lang="en">

<head>
<?php
		include('views/template/adminheader.php');
	?>
	<title>Poojashakti Dashboard</title>


</head>

<body>

<?php
		include('views/template/adminnav.php');
	?>

		<div class="main-panel">
			<nav class="navbar navbar-default navbar-fixed">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#">Dashboard</a>
					</div>
					<?php 
						if(!empty($_SESSION["id"])){
							include('views/template/logout.php');
						}else{
							echo '';
						};
						if(!empty($_SESSION['id'])){
					?>
				</nav>

			<!--CUSTOM-->

			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-4">
							<a href="customer-details.php">
								<div class="card">
									<div class="header">
										<h4 class="title">Total Customer</h4>
										<p class="category">Details views</p><br>
									</div>
									<div class="iconcust text-center" style="background-color: grey;">
										<i class="pe-7s-users"></i>
									</div>
								</div>
							</a>
						</div>
						<div class="col-md-4">
							<a href="seller-details.php">
								<div class="card">
									<div class="header">
										<h4 class="title">Total Seller</h4>
										<p class="category">Details views</p><br>
									</div>
									<div class="iconcust text-center">
										<i class="pe-7s-users"></i>
									</div>
								</div>
							</a>
						</div>
						<div class="col-md-4">
						<a href="revenue.php">
							<div class="card">
								<div class="header">
									<h4 class="title">Revenue</h4>
									<p class="category">Revenue From The Sale Products</p><br>
								</div>
								<div class="iconcust2 text-center">
									<i class="pe-7s-cash"></i>
								</div>
								
							</div>
							</a>
						</div>
						<div class="col-md-4">
							<a href="product-details.php">	
								<div class="card">
									<div class="header">
										<h4 class="title">Products</h4>
										<p class="category">Product for sale</p><br>
									</div>
									<div class="iconcust3 text-center">
										<i class="pe-7s-server"></i>
									</div>
									
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php
						}else{
				echo '<center><a href="login-dashboard.php" class="btn btn-primary" >LOGIN</a><center>';
				echo '<img src="https://previews.123rf.com/images/cougarsan/cougarsan1709/cougarsan170900015/85028916-the-emoji-yellow-disappointed-upset-face-and-closing-eyes-icon.jpg" height="400" width="500" ';
				}
			?>
</body>

<!--   Core JS Files   -->
<script src="dashboard/assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="dashboard/assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="dashboard/assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="dashboard/assets/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="dashboard/assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="dashboard/assets/js/demo.js"></script>

<script type="text/javascript">
	$(document).ready(function() {

		demo.initChartist();

		$.notify({
			icon: 'pe-7s-gift',
			message: "Welcome to <b>Poojashakti Dashboard</b>."

		}, {
			type: 'info',
			timer: 4000
		});

	});
</script></html>
