<div class="wrapper">
		<div class="sidebar" data-color="purple" data-image="assets/img/sidebar-5.jpg">

			<!--   you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple" -->


			<div class="sidebar-wrapper">
				<div class="logo">
					<a href="http://www.creative-tim.com" class="simple-text">
						POOJASHAKTI
					</a>
				</div>

				<ul class="nav">
					<li>
						<a href="dashboard.php">
							<i class="pe-7s-graph"></i>
							<p>Dashboard</p>
						</a>
					</li>
					<li>
						<a href="user.php">
							<i class="pe-7s-user"></i>
							<p>User Profile</p>
						</a>
					</li>
					<li>
						<a href="approve-reject-product.php">
							<i class="pe-7s-way"></i>
							<p>Approve / Reject</p>
						</a>
					</li>
					<li>
						<a href="sellers-account.php">
							<i class="pe-7s-add-user"></i>
							<p>Add Seller's Account</p>
						</a>
					</li>
					<li >
						<a href="view-orders.php">
							<i class="pe-7s-note2"></i>
							<p>View Orders</p>
						</a>
					</li>
					<li >
						<a href="product-items.php">
						<i class="pe-7s-cloud-upload"></i>
							<p>Upload Items</p>
						</a>
					</li>
					
				</ul>
			</div>
		</div>