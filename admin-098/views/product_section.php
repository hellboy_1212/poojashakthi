<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="header">
									<h2 class="title" style="font-weight: bold">Upload Product Details on Products Page - General Item</h2><br>
									<p class="category"><u>Poojashakti.com</u>- Products Page</p>
									<hr>
								</div>
								<div class="content">
								
								<form class="form-signin" action="" method="post" enctype="multipart/form-data">
											
											<div class="form-label-group">
												<input type="hidden" name="product_id" class="form-control">
											</div>

											<div class="form-label-group">
												<label for="exampleFormControlTextarea1">Product Name:</label>
												<input type="text" id="text" name="product_name" class="form-control" required placeholder="Full name of the product">
											</div>
										
											<div class="form-label-group">
												<label for="exampleFormControlTextarea1">Category id:</label>&nbsp;&nbsp;&nbsp;&nbsp;
												<select name="category_id">
													<option value="1">Bag</option>
													<option value="2">Mobile</option>
													<option value="3">Game</option>
													<option value="4">Mobile_woo</option>
													<option value="5">Mob</option>
												</select><br />
											</div>

											<div class="form-label-group">
											<label for="exampleFormControlTextarea1">Choice option where you want to show your product </label><br />
											<br />
											<p style="color: red">NOTE:</p>
												<p style='color: darkgrey'>NO // Not to show product in that page  && YES // Yes to show this product in that page</p>
												<p style='color: green'>**** Default value will be NO which add product in product page *****</p><br />
											<label>Special</label>&nbsp;&nbsp;
											<select name="special">
												<option value="0">No</option>
												<option value="1">Yes</option>
											</select><br />

											<label>Featuer</label>&nbsp;&nbsp;
											<select name="feature">
												<option value="0">No</option>
												<option value="1">Yes</option>
											</select>
											</div>
							
											<div class="form-label-group">
												<label for="exampleFormControlTextarea1">Product Discription:</label>
												<textarea class="form-control" name="product_description" id="exampleFormControlTextarea1" rows="4" required></textarea>
											</div>

											<div class="form-label-group">
												<label for="exampleFormControlTextarea1">Product image:</label>
												<input type="file" id="text" name="pic" class="form-control" required placeholder="image of the product">
											</div>

											<div class="form-label-group">
												<label for="exampleFormControlTextarea1">Product price:</label>
												<input type="number" id="text" name="product_actualprice" class="form-control" required placeholder="Price of the product in RS">
											</div>

											<div class="form-label-group">
												<label for="exampleFormControlTextarea1">Product sale Price:</label>
												<input type="number" id="text" name="product_price" class="form-control" required placeholder="Price of the product in RS">
											</div>

											<div class="text-center">
											<br>
												<button class="btn btn-info btn-lg btn-fill text-uppercase" type="submit" name="submit" >UPLOAD</button>
											</div>
											<hr class="my-4">
									</form>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>