<?php
	session_start();
?>
<!doctype html>
<html lang="en">

<head>
<?php
		include('views/template/adminheader.php');
	?>
	<title>Dashboard - Login</title>

</head>

<body>
	<div class="wrapper">
		<div class="panel">
			<nav class="navbar navbar-default navbar-fixed">
				<div class="container-fluid">
					<div class="navbar-header">
						<a class="navbar-brand" href="#">Poojashakti Dashboard</a>
					</div>
				</div>
			</nav>
			<!--CUSTOM-->
			<div class="content signincnt">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-4">
						</div>
						<div class="col-md-4">
							<div>
								<div class="card-body-signin text-center">
									<h3 class="loogintxt">
										DASHBOARD LOGIN</h3>
									<form class="form-signin" method="post">
										<div class="form-label-group">
											<input type="text" id="inputEmail" class="form-control" name="email" placeholder="UserName" required autofocus>
										</div>
										<div class="form-label-group">
											<input type="password" id="inputPassword" name="pass" class="form-control" placeholder="Password" required>
										</div>
										<button class="btn btn-md btn-fill signinbtn btn-block text-uppercase" name="submit" type="submit">Sign in</button>

										<?php
											$conn=mysqli_connect("localhost","root","");
											mysqli_select_db($conn, "pshakthi");
											if(isset($_POST['submit']))	
											{
												$email = mysqli_real_escape_string($conn,$_POST['email']);
												$pass = mysqli_real_escape_string($conn,$_POST['pass']);

												$sql="SELECT * from admin where email='$email' and pass='$pass'";

												$result = mysqli_query($conn,$sql);
												$row = mysqli_fetch_array($result,MYSQLI_ASSOC);

												$count = mysqli_num_rows($result);
												if($count == 1) {
													$_SESSION["id"] = $row["id"];
													header('Location: dashboard.php');
												}else {
												
													echo '<script language="javascript">alert("Your Login Name or Password is invalid")</script>';
													echo "<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>'";
													echo '<div class="col-12 col-lg-10"style="background-color: lightgrey; margin-top:100px">';
													echo '<center style="margin-top:200px;"><p><h2 style="color: red;">Click here to go back <a href=" login-dashboard.php" style="color: green;">LOGIN</a> page again</h2></p></center>';
													echo "<center><i class='fas fa-dizzy' style='font-size:200px;color:red'></i></center>";
													echo '</div>';
												
												}
											}
											?>
									</form>
								</div>
							</div>
						</div>
						<div class="col-md-4">
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</body>
<!--   Core JS Files   -->
<script src="dashboard/assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="dashboard/assets/js/bootstrap.min.js" type="text/javascript"></script>
<!--  Charts Plugin -->
<script src="dashboard/assets/js/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="dashboard/assets/js/bootstrap-notify.js"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="dashboard/assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>
<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="dashboard/assets/js/demo.js"></script>
<script type="text/javascript">
	$(document).ready(function() {

		demo.initChartist();

		$.notify({
			icon: 'pe-7s-gift',
			message: "<strong>LOGIN PAGE</strong>- Login with your credentials."

		}, {
			type: 'info',
			timer: 4000
		});

	});
</script></html>
